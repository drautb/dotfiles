#!/usr/bin/env sh

#
# Main installation script.
# Symlinks the important files to ~, installs software, sets defaults.
#

# Symlink dotfiles
ln -sh ~/GitHub/drautb/dotfiles/.astylerc_java ~/.astylerc_java
ln -sh ~/GitHub/drautb/dotfiles/.gitconfig ~/.gitconfig
ln -sh ~/GitHub/drautb/dotfiles/.m2 ~/.m2
ln -sh ~/GitHub/drautb/dotfiles/.ssh ~/.ssh
ln -sh ~/GitHub/drautb/dotfiles/.zshrc ~/.zshrc
ln -sh ~/GitHub/drautb/dotfiles/bin ~/bin

sudo cp init/krb5.conf /etc/krb5.conf
sudo chmod a+r /etc/krb5.conf

# Install homebrew
if [ ! "$(which brew)" ]
  then ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Install ZSH
if [ "$(which zsh)" != "/usr/local/bin/zsh" ]
  then
    brew install zsh
    chsh -s /usr/local/bin/zsh
fi

# Install oh-my-zsh
if [ ! -d "$HOME/.oh-my-zsh" ]
  then sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
fi

# Install rvm
if [ ! "$(which rvm)" ]
  then
    brew install gpg
    gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
    curl -sSL https://get.rvm.io | bash -s stab
fi

# Install apps with homebrew
brew install caskroom/cask/brew-cask
brew tap caskroom/versions

brew cask install alfred
brew cask install dash
brew cask install dropbox
brew cask install firefox
brew cask install google-chrome
brew cask install heroku-toolbelt
brew cask install intellij-idea
brew cask install iterm2
brew cask install java6
brew cask install java7
brew cask install keepassx
brew cask install racket
brew cask install scroll-reverser
brew cask install silverlight
brew cask install slack
brew cask install spectacle
brew cask install sublime-text3
brew cask install ynab

# Install other homebrew stuff
brew install awscli
brew install cassandra
brew install git
brew install golang
brew install haproxy
brew install jq
brew install maven
brew install mysql
brew install ncdu
brew install nvm
brew install pgcli
brew install shellcheck
brew install wget
brew install youtube-dl

# Sublime Text

# Install package control
if [ ! -e "$HOME/Library/Application Support/Sublime Text 3/Installed Packages/Package Control.sublime-package" ]
  then curl -o "$HOME/Library/Application Support/Sublime Text 3/Installed Packages/Package Control.sublime-package" "https://packagecontrol.io/Package%20Control.sublime-package"
fi

# Import all my package settings
ln -sh "$HOME/GitHub/drautb/dotfiles/init/Markdown.sublime-settings" "$HOME/Library/Application Support/Sublime Text 3/Packages/User/Markdown.sublime-settings"
ln -sh "$HOME/GitHub/drautb/dotfiles/init/Package Control.sublime-settings" "$HOME/Library/Application Support/Sublime Text 3/Packages/User/Package Control.sublime-settings"
ln -sh "$HOME/GitHub/drautb/dotfiles/init/RubyTest.sublime-settings" "$HOME/Library/Application Support/Sublime Text 3/Packages/User/RubyTest.sublime-settings"
ln -sh "$HOME/GitHub/drautb/dotfiles/init/SublimeAStyleFormatter.sublime-settings" "$HOME/Library/Application Support/Sublime Text 3/Packages/User/SublimeAStyleFormatter.sublime-settings"
ln -sh "$HOME/GitHub/drautb/dotfiles/init/SublimeLinter.sublime-settings" "$HOME/Library/Application Support/Sublime Text 3/Packages/User/SublimeLinter.sublime-settings"

# Install Sublime Text settings
ln -sh "$HOME/GitHub/drautb/dotfiles/init/Preferences.sublime-settings" "$HOME/Library/Application Support/Sublime Text 3/Packages/User/Preferences.sublime-settings"
ln -sh "$HOME/GitHub/drautb/dotfiles/init/Default (OSX).sublime-keymap" "$HOME/Library/Application Support/Sublime Text 3/Packages/User/Default (OSX).sublime-keymap"

# Download remove-unused-imports script.
if [ ! "$(which remove-unused-imports)" ]
  then curl -o ~/bin/remove-unused-imports https://gist.githubusercontent.com/rodrigosetti/4734557/raw
fi
chmod a+x ~/bin/remove-unused-imports
