#!/usr/bin/env bash

# development-fh5
aws s3 cp s3://FH5-security/us-east-1/vpc-instance.pem ~/.ssh/fh5-useast1-vpc-instance.pem --profile fh5-us-east-1
aws s3 cp s3://account-info-074150922133/superadmin/us-east-1/vpc-bastion-150421.pem ~/.ssh/fh5-useast1-vpc-bastion.pem --profile fh5-us-east-1
aws s3 cp s3://account-info-074150922133/superadmin/eu-central-1/vpc-bastion-150421.pem ~/.ssh/fh5-eucentral1-vpc-bastion.pem --profile fh5-us-east-1

# test-fh3
aws s3 cp s3://FH3-security/us-east-1/vpc-instance.pem ~/.ssh/fh3-useast1-vpc-instance.pem --profile fh3-us-east-1
aws s3 cp s3://account-info-643055571372/superadmin/us-east-1/vpc-bastion-150421.pem ~/.ssh/fh3-useast1-vpc-bastion.pem --profile fh3-us-east-1
aws s3 cp s3://account-info-643055571372/superadmin/eu-central-1/vpc-bastion-150421.pem ~/.ssh/fh3-eucentral1-vpc-bastion.pem --profile fh3-us-east-1

# production-fh1
aws s3 cp s3://FH1-security/us-east-1/vpc-instance.pem ~/.ssh/fh1-useast1-vpc-instance.pem --profile fh1-us-east-1
aws s3 cp s3://account-info-914248642252/superadmin/us-east-1/vpc-bastion-150422.pem ~/.ssh/fh1-useast1-vpc-bastion.pem --profile fh1-us-east-1
aws s3 cp s3://account-info-914248642252/superadmin/eu-central-1/vpc-bastion-150421.pem ~/.ssh/fh1-eucentral1-vpc-bastion.pem --profile fh1-us-east-1
aws s3 cp s3://account-info-914248642252/superadmin/eu-west-1/vpc-bastion-150421.pem ~/.ssh/fh1-euwest1-vpc-bastion.pem --profile fh1-us-east-1

chmod 600 ~/.ssh/*.pem
