# Start fresh
DROP DATABASE IF EXISTS sonar;
CREATE DATABASE sonar CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE USER 'sonar' IDENTIFIED BY 'sonar';
GRANT ALL PRIVILEGES ON sonar.* to 'sonar'@'%' IDENTIFIED BY 'sonar';
GRANT ALL PRIVILEGES ON sonar.* to 'sonar'@'localhost' IDENTIFIED BY 'sonar';
FLUSH PRIVILEGES;
