dotfiles
========

Stored configuration/setup scripts for my laptop at FamilySearch.

1. `setup-osx`
2. `./install.sh`
3. Start `mysqld` and `./install-sonar.sh`
4. `.ssh/fs-eng-download-keys.sh`

Set my work email address. Run this in ~/GitHub/fs-eng/ after the code is cloned.

`find . -maxdepth 1 -type d \( ! -name . \) -exec bash -c "cd '{}' && git config --local user.email \"drautb@familysearch.org\"" \;`