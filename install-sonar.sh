#!/usr/bin/env bash

# Assumes mysql is already installed/running.
mysql -u root < init/sonar.sql

# Download Sonar
curl -o "/tmp/sonarqube-4.5.4.zip" http://rptbit01.a.fsglobal.net/sonar/sonarqube-4.5.4.zip

mkdir "$HOME/sonar"
unzip "/tmp/sonarqube-4.5.4.zip" -d "$HOME/sonar"
ln -s "$HOME/sonar/sonarqube-4.5.4" "$HOME/sonar/sonar"
rm "/tmp/sonarqube-4.5.4.zip"

# Install driver
scp rptbit01.a.fsglobal.net:/var/www/sonar/mysql-jdbcDriver-5.1.27.tgz /tmp
tar -xzvf /tmp/mysql-jdbcDriver-5.1.27.tgz -C ~/sonar/sonar/extensions/jdbc-driver

# Copy configuration files.
cp init/sonar.properties ~/sonar/sonar/conf/sonar.properties
cp init/wrapper.conf ~/sonar/sonar/conf/wrapper.conf

# Start SONAR by doing `rvm use system && ~/sonar/sonar/bin/macosx-universal-64/sonar.sh console`
# Install XML, PMD and Checkstyle plugins.

# Load ruleset from FHD.
scp rptbit01.a.fsglobal.net:/var/www/sonar/SonarQube454WayWithFindbugsForFSJava3-1.xml ~/sonar
