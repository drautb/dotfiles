#!/usr/bin/env ruby

require "benchmark"
require "net/http"
require "uri"
require "json"
require "optparse"
require "ostruct"
require "pp"

# An fsglobal-to-aws client.
# It will login in and gather AWS credentials
# and save them into 3 locations:
# * aws config file
# * env files that can be sourced.
# * sas properties file (so that paas-sysps-ruby will work)
class FSGlobalToAwsCli

  BASE_URI = "https://dptservices.familysearch.org"
  API_PATH = "/aws/api"
  LOGIN_URI = "#{API_PATH}/login"
  CREDENTIAL_URI = "#{API_PATH}/iam"

  DEFAULT_USERNAME = `whoami`.chomp
  DEFAULT_ACCOUNT = "all"
  DEFAULT_ROLE = "SuperAdmin"
  DEFAULT_PROFILE = "fh5-us-east-1"

  ONE_DAY = 86400
  ONE_HOUR = 3600

  def initialize
    @options = parse_args(ARGV)
    log("Using options: #{@options}")

    @http = init_http
    @password = get_password
  end

  def login
    startTime = Time.now
    loop do
      login_to_portal
      generate_credentials

      puts "\nWill gen new credentials @ #{Time.now + ONE_HOUR} -- Press Ctrl+C to stop immediately.:w"
      sleep ONE_HOUR

      break if Time.now - startTime > ONE_DAY
    end
  end

  private

  def login_to_portal
    log("Logging in to AWS...")
    request = Net::HTTP::Post.new(LOGIN_URI, initheader = {'Content-Type' => 'application/json'})
    request.body = {"username" => @options.username, "password" => @password}.to_json
    response = nil
    time = Benchmark.realtime {
      response = @http.request(request)
    }
    log("Received in time=#{time} response: #{response}")
    unless response.code == "200"
      puts "Could not login to portal!"
      exit
    end
    @cookie = response["set-cookie"]
  end

  def generate_credentails_for_account(account)
    short_account_id = extract_short_account_id(account)
    creds = get_aws_credentials(account)
    log("Received account=#{account} credentials: #{creds}")

    creds["account_id"] = account
    creds["short_account_id"] = short_account_id

    creds
  end

  def get_aws_credentials(account_id)
    log("Fetching credentials...")
    uri = "#{CREDENTIAL_URI}/#{account_id}/#{@options.role}"
    request = Net::HTTP::Put.new(uri, initheader = {'Content-Type' => 'application/json'})
    request["Cookie"] = @cookie
    response = nil
    time = Benchmark.realtime {
      response = @http.request(request)
    }
    log("Received time=#{time} response: #{response}")
    unless response.code == "200"
      puts "Could not fetch credentials for account_id=#{account_id}!"
      return {}
    end
    response_hash = JSON.parse(response.body)
    response_hash["body"]
  end

  def generate_credentials
    account_creds = {}
    @options.accounts.each do |account|
      puts "Generating credentials for #{account}"
      account_creds[account] = generate_credentails_for_account(account)
    end

    save_to_files(account_creds)
  end


  def fixup_login_url(url, region)
    match_data = url.match /^(?<url>.*)region=(?<region>.*)$/
    match_data["url"] + "region=#{region}"
  end

  def generate_env_data (creds, region)
    loginUrl = fixup_login_url(creds["loginUrl"], region)
    output = "export AWS_ACCOUNT_NAME=\"#{creds["account_id"]}\"\n" +
             "export AWS_USERNAME=\"#{creds["username"]}\"\n" +
             "export AWS_PASSWORD=\"#{creds["password"]}\"\n" +
             "export AWS_ACCESS_KEY_ID=\"#{creds["accessKeyId"]}\"\n" +
             "export AWS_SECRET_ACCESS_KEY=\"#{creds["secretAccessKey"]}\"\n" +
             "export AWS_SECRET_KEY=\"#{creds["secretAccessKey"]}\"\n" +
             "export AWS_DEFAULT_REGION=\"#{region}\"\n"+
             "export AWS_LOGIN_URL=\"#{loginUrl}\"\n"
    output
  end

  def generate_sas_client_data (creds)
    output = "fchUser=#{creds["accessKeyId"]}\n" +
             "fchPassword=#{creds["secretAccessKey"]}\n"
    output
  end

  def generate_aws_config_profile(region, creds)
    profile_name = "#{creds["short_account_id"]}-#{region}"
    profile ="[profile #{profile_name}]\n" +
             "region=#{region}\n" +
             "output=json\n\n"
    if profile_name == @options.default_profile
      profile += "# Default matches: #{profile_name}\n" +
                 "# If you want to change that use the --d #{profile_name} option.\n" +
                 "# using the profile that would prefer to be the default.\n" +
                 "[default]\n" +
                 "region=#{region}\n" +
                 "output=json\n\n"
    end
    profile
  end

  def generate_aws_config (account_creds)
    output = ""
    account_creds.each do |account, creds|
      regions = get_regions_for_account(account)
      regions.each do |region|
        output += generate_aws_config_profile(region, creds)
      end
    end

    output
  end

  def generate_aws_credentials_profile(region, creds)
    profile_name = "#{creds["short_account_id"]}-#{region}"
    profile ="[#{profile_name}]\n" +
             "aws_access_key_id=#{creds["accessKeyId"]}\n" +
             "aws_secret_access_key=#{creds["secretAccessKey"]}\n\n"
    if profile_name == @options.default_profile
      profile += "[default]\n" +
                 "aws_access_key_id=#{creds["accessKeyId"]}\n" +
                 "aws_secret_access_key=#{creds["secretAccessKey"]}\n\n"
    end
    profile
  end

  def generate_aws_credentials(account_creds)
    output = ""
    account_creds.each do |account, creds|
      regions = get_regions_for_account(account)
      regions.each do |region|
        output += generate_aws_credentials_profile(region, creds)
      end
    end

    output
  end

  def save_output_to_file(output, filename, file_mode="w")
    File.open(filename, file_mode) do |file|
      file.write(output)
    end
    puts "Credentials saved to #{filename}"
    log(output)
  end

  def save_to_files(account_creds)
    save_sas_client(account_creds)
    save_aws(account_creds)
    save_envs(account_creds)
  end

  def save_sas_client(account_creds)
    account_creds.each do |account, creds|
      output = generate_sas_client_data(creds)
      save_output_to_file(output, ENV["HOME"] + "/.sas/paas-sysps-aws-#{creds["short_account_id"]}-db")
    end
  end

  def get_regions_for_account(account)
    regions = []
    regions << "us-east-1"
    if account == "production-fh1"
      regions << "eu-west-1"
    end
    regions
  end

  def save_aws(account_creds)
    output = generate_aws_config(account_creds)
    save_output_to_file(output, ENV["HOME"] + "/.aws/config")
    output = generate_aws_credentials(account_creds)
    save_output_to_file(output, ENV["HOME"] + "/.aws/credentials")
  end

  def save_envs(account_creds)
    account_creds.each do |account, creds|
      regions = get_regions_for_account(account)
      regions.each do |region|
        save_env(creds, region)
      end
    end
  end

  def save_env(creds, region)
    output = generate_env_data(creds, region)
    puts "---------------------------------------------------------------------"
    save_output_to_file(output, "#{ENV['HOME']}/#{creds["account_id"]}-#{region}-env")
    puts output
  end

  def get_password
    print "Password: "
    system "stty -echo"
    password = gets.chomp
    system "stty echo"
    puts
    password
  end

  def init_http
    uri = URI.parse(BASE_URI)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http
  end

  def parse_args (args)
    options = OpenStruct.new

    options.username = DEFAULT_USERNAME
    options.account = DEFAULT_ACCOUNT
    options.role = DEFAULT_ROLE
    options.default_profile = DEFAULT_PROFILE
    options.verbose = false
    account_description = "AWS account. (Default: #{options.account})\n\t'all' includes #{extract_account(options.account)}"

    opts = OptionParser.new do |opts|
      opts.banner =  "Usage: fsglobal-to-aws-cli [options]"

      opts.separator ""
      opts.separator "Specific Options:"

      opts.on("-v", "--verbose", "Run verbosely") do |verbose|
        options.verbose = verbose
      end

      opts.on("-u", "--user STR", "Your FSGlobal username. (Default: #{options.username})") do |username|
        options.username = username
      end

      opts.on("-a", "--account STR", account_description) do |account|
        options.account = account
      end

      opts.on("-r", "--role STR", "The Role you want to assume in AWS. (Default: #{options.role})\n\tThis client assumes you know the list and that you are authorized.") do |role|
        options.role = role
      end

      opts.on("-d", "--default STR", "Set what the default profile will use.  (Default: #{options.default_profile}\n\tOthers might be fh1-us-east-1 or fh1-eu-west-1 etc.") do |profile|
        options.default_profile = profile
      end

      opts.on("-h", "--help", "Show this message") do
        puts opts
        exit
      end

      opts.separator ""
      opts.separator "Purpose:"
      opts.separator "    Will login into the portal and generate credentials."
      opts.separator "    And place the credentials in these locations:"
      opts.separator "      aws config"
      opts.separator "      env files to source from"
      opts.separator "      .sas/<properties files> for paas-sysps-ruby"
      opts.separator "    The full path location will be provided when run."
      opts.separator ""
      opts.separator "Inspired by:  \n    Ben Draut -- initial code.\n    Brent Eggleston -- Do multiple accounts at once."

      opts.parse!(args)
      opts
    end

    options.accounts = extract_account(options.account)

    options
  end

  def extract_account(account)
    if account == "all"
      ["development-fh5","test-fh3","production-fh1"]
    else
      [account]
    end
  end

  def extract_short_account_id(account)
    log("account=#{account}\n")
    match_data = /^(?<aws_account_type>[a-zA-Z]+\-)(?<account_id>fh\d+)$/i.match(account)
    raise("The given account '#{account}' is not valid") if match_data.nil?

    match_data["account_id"]
  end

  def extract_region(region_data)
    match_data = /^(?<country>[a-zA-Z]{2})(?<direction>[a-zA-Z\-]+)(?<number>\d+)$/i.match(region_data)
    "#{match_data[:country]}-#{match_data[:direction]}-#{match_data[:number]}"
  end


  def log (msg)
    puts msg if @options.verbose
  end

end

client = FSGlobalToAwsCli.new
client.login